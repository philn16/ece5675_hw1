#!/usr/bin/env python
from sympy import symbols, solve, Abs, I, oo, pi, N, sqrt, nsolve, simplify, expand, apart, numer, denom, together, Poly, atan, re, im, atan2
import sys

r1,r2s,r2p,c,w=symbols('r1 r2s r2p c w',positive=True)
s=symbols('s')
tau1,tau2,wn,zeta=symbols('tau1 tau2 wn zeta',positive=True)

def abc_print(pol : list,description=''):
	""" conviniece function for printing a) poly[0], b) poly[1], ... """ 
	print("")
	print("a)"+description+str(pol[0]))
	if len(pol) > 1:
		print("b)"+description+str(pol[1]))
	if len(pol) > 2:
		print("c)"+description+str(pol[2]))

def sep_terms(func):
	""" makes the numerator and denominator cleanely seperate by 's' """
	return apart(numer(func),s)/apart(denom(func),s)

def get_h(fs):
	""" return the transfer function from the loop filter """
	return sep_terms(together(fs/(s+fs)))

def get_g(fs):
	""" return the open loop gain from the loop filter """
	return simplify(fs/s)

# the loop filter functions are defined by the problem
fs=[(1+s*tau2)/(s*tau1),1/(1+s*tau1),(1+s*tau2)/(1+s*tau1)]

# find the transfer functions
hs=[get_h(fsx) for fsx in fs]
# print the transfer functions
abc_print(hs," H(s) = ")

def find_wc(gain):
	""" takes a gain function of s and returns the unity gain radial frequency """
	gain=gain.subs({s:I*w})
	solutions=solve(abs(gain)-1,w)
	subs={tau1:1,tau2:1}
	ret=[]
	for solution in solutions:
		val=N(solution.subs(subs))
		if val.is_positive:
			ret.append(solution)
	assert len(ret) == 1, "length of solutions is %d"%len(ret)
	return ret[0]

# get the open loop gain
gs=[get_g(fsx) for fsx in fs]
# find the various unity gain radial frequencies
wc=[find_wc(gsx) for gsx in gs]

# print the unity gain radial freqeuencyies
abc_print(wc," Wc   = ")

def get_subs(hs):
	""" for a given transfer function this will solve for tau_1 and tau_2 in terms of the damping factor and natural frequency. returns the subsitudtion as a dictionary. """
	den=denom(hs)
	a,b,c=Poly(den,s).all_coeffs()
	b,c=b/a,c/a
	sol=solve([2*zeta*wn-b,c-wn**2],[tau1,tau2],dict=True)
	assert len(sol) == 1, "got %d solutions instead of 1 for H(s) = %s"%(len(sol),hs)
	return sol[0]

# solve for the damping factor and natural frequency
# problem b is a special case since it doesn't have tau_2, and was done manually
subs=[get_subs(hs[0]) ,{tau1:1/(2*zeta)**2} , get_subs(hs[2])]
# print the subsitutions
abc_print(subs,' subs = ')


# subsitude in values of zeta and wn for tau1 and tau2
# we're also subsituting s for j wc
gs_subs=[gsx.subs({s:I*wcx}) for wcx,gsx in zip(wc,gs)]
gs_subs=[simplify(gsx.subs(subsx)) for gsx,subsx in zip(gs_subs,subs)]

# print the unity gains
abc_print(gs_subs,' unity_open_loop_gain(zeta,wn) = ')

# calculate the phase margins
phi_m=[pi+atan2(im(gs_subsx),re(gs_subsx)) for gs_subsx in gs_subs[:2]]

# more manual stuff
real=-2*zeta**2+sqrt(4*zeta**4 + 1)
imag=2*zeta*sqrt(-2*zeta**2 + sqrt(4*zeta**4 + 1))
phi_m[1]=pi+atan2(imag,real)

# simplify what we can.
for i in [0,1]:
	phi_m[i]=simplify(phi_m[i])
# print the phase margins
abc_print(phi_m[:2]," phi_m = ")