#!/usr/bin/env python
from sympy import symbols, solve, Abs, I, oo, pi, N, sqrt, nsolve, simplify, expand, apart, numer, denom, together, Poly, im, re
import sys

r1,r2s,r2p,c,w,s=symbols('r1 r2s r2p c w s',positive=True)

z1=r2s+1/(s*c+1/r2p )
z1=together(z1)
fs=-z1/r1
gs=1/s*fs
hs = together(gs/(1+gs))

hs=apart(numer(hs),s)/apart(denom(hs),s)
print("H(s) = "+str(hs))

den=denom(hs)
num=numer(hs)

# pole at 20 Hz - the imaginary part of the quadratic is 20
def get_pole_eq():
	a,b,c=Poly(den,s).all_coeffs()
	return (c/a-(b/(2*a))**2) - 400
pole_equation=get_pole_eq()

# "zero" at 100 Hz
expanded_numerator=num.subs({s:I*100*2*pi})
zero_equation=im(expanded_numerator)-re(expanded_numerator)

# construct system of equations
equations=[
	pole_equation, # pole at 20 Hz
	r1-1000, # given
	(r2s+r2p)/r1-100, # DC gain of 100
	zero_equation, # zero at 100 Hz
]

solutions=nsolve(equations,[c,r2s,r2p,r1],[1e-6,100,101,1000])
# solutions=solve(equations,[c,r2s,r2p,r1],dict=True)[0]

print(solutions)

## -------- end



# Poly(den,s).all_coeffs()
# Poly(num,s).all_coeffs()

# equations to solve
# zero_at_100_hz=numer(fs)
# dc_gain_100=fs.subs({s:oo})

