#!/usr/bin/env python
from sympy import symbols, solve, Abs, I, oo, pi, N, sqrt, nsolve
r1,r2,c,w=symbols('r1 r2 c w',reals=True)

z1=r2+1/(I*w*c)
gain_n=z1/r1

gain_high=gain_n.subs({w:oo,r1:1000, r2:5000})
gain_1=N(abs(gain_n.subs({w:2*pi, r1:1000, r2:5000, c:1.593542605e-6})))

print("high freq gain: "+str(gain_high))
print("gain at 1 Hz:   "+str(gain_1))

r1=1000
r2=5000
g=100
print(solve(r2**2+(2*pi*c)**(-2)-(100*r1)**2,c))