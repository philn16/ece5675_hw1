#!/usr/bin/env python
import numpy as np

def get_open_loop_gain_f(kp=0.1 / (2*np.pi),klf=3,kv=1e6*(2*np.pi)):
	gain_open_loop_s=lambda s: kp*klf*kv*1/s
	gain_open_loop_f=lambda f: abs(gain_open_loop_s(1j*2*np.pi*f))
	return gain_open_loop_f
print("open-loop gain at 47.75 KHz is %g"%(get_open_loop_gain_f()(47.75e3)))


#!/usr/bin/env python
from sympy import nsolve, symbols, pi, I
def solve_unity_gain_freq(kp=1):
	# assign known variables
	klf=3;kv=1e6*2*pi
	w,s,f=symbols('w s f')
	gain_open_loop_s=kp*klf*kv*1/s
	gain_open_loop_f=abs(gain_open_loop_s.subs({s:1j*2*pi*f}))
	f_unity_gain=nsolve(gain_open_loop_f-1,f,50e3)
	return f_unity_gain
# solution to 1b
print("unity gain frequency is %.9g Hz"%solve_unity_gain_freq(1))
# solution to 1a
print("unity gain frequency is %g Hz"%solve_unity_gain_freq(.1/(2*pi)))

