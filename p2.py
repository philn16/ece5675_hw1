#!/usr/bin/env python
from sympy import symbols, simplify, pi
from numpy import angle
w,s,c,z1,r1,r2=symbols('w s c z1 r1 r2')
z1=r2+1/(s*c)
f = z1/(r2+z1)
f=simplify(f)
subs={c:1e-6, r1:1000, r2:100}
print("2a) f(s)=%s"%f.subs(subs))
w=2*pi*500
subs.update({s:1j*w})
gain=abs(f.subs(subs))
phase=angle(complex(f.subs(subs)))
print("2b) Gain: %g, Angle: %g, %g deg"%(gain,phase,phase*180/pi))